{ config, pkgs, nix, ... }:

{
    # services.xserver.enable = true;

    # KDE
    # services.xserver.displayManager.sddm.enable = true;
    # services.xserver.desktopManager.plasma5.enable = true;

    # Enable GNOME applications
    services.xserver = {
	enable = true;

	desktopManager.gnome = {
		enable = true;
	}; 

	displayManager = {
		gdm.enable = true;
	};

	# Enable default GNOME display manager
	# Also add River to the available login sessions
	# displayManager = {
	# 	gdm.enable = true;

	# 	session = [
	# 		{
	# 			manage = "desktop";
	# 			name = "River";
	# 			start = ''
	# 				export XDG_SESSION_TYPE="wayland"
	# 				export XDG_CURRENT_DESKTOP="Unity"
	# 				export XKB_DEFAULT_LAYOUT="dk"

	# 				dbus-run-session river &
	# 			'';
	# 		}
	# 	];
	# };

	# Disable the default display manager and remain in tty
	# displayManager.startx.enable = true;
    };

    environment.systemPackages = with pkgs;
    with gnomeExtensions; [
      gnome.dconf-editor
      gnome.gnome-tweaks
      gnome.gnome-shell-extensions # required for user-theme
      dash-to-panel
      appindicator
      pop-shell
    ];

    # Exclude some applications
    environment.gnome.excludePackages = with pkgs; [
      gnome.gnome-weather
      gnome.gnome-calendar
      gnome.gnome-maps
      gnome.gnome-contacts
      gnome.gnome-software
      gnome.totem
      gnome.epiphany
    ];
    programs.gnome-terminal.enable = false;
}
