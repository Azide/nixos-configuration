{ config, pkgs, ... }:

{
    imports = [
        ./hardware-configuration.nix
        ./base.nix
        ./users.nix
        ./packages.nix
        ./desktop.nix
        ./bootloader.nix
    ];

    system.stateVersion = "21.05";
}
