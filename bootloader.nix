{ config, pkgs, ... }:

{
    boot.loader.grub.enable = true;
    boot.loader.grub.version = 2;

    # Dual boot
    # boot.loader.grub.useOSProber = true;

    # Efi boot
    boot.loader.grub.efiSupport = true;
    boot.loader.grub.efiInstallAsRemovable = true;

    boot.loader.efi.efiSysMountPoint = "/boot/efi";
    boot.loader.efi.canTouchEfiVariables = false;

    # Which drive to install grub
    boot.loader.grub.device = "nodev";

    # Install Plymouth for that sexy boot splashscreen
    boot.plymouth.enable = true;
}
