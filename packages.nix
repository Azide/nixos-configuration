{ config, pkgs, ... }:

{
    nixpkgs.config = {
        allowUnfree = true;
    };

    # dwl
    nixpkgs.config.packageOverrides = pkgs: {
	dwl = pkgs.dwl.override { 
		conf = /home/marcus/.config/dwl/config.h; 
		patches = [ 
			/home/marcus/.config/dwl/patches/cursortheme.patch
			/home/marcus/.config/dwl/patches/smartborders.patch
		]; 
	};
    };

    environment.systemPackages = with pkgs; [
    	killall
	gnumake
	gcc
        neovim
        wget
        exa
        bat
        fd
        ripgrep
        nnn
	unzip
	fzf
        firefox
        discord-canary
        git
        neofetch
	pfetch
        vscode-with-extensions
        bitwarden
        libreoffice
	megasync
	wayland
	wayland-utils
	libappindicator
        kanshi
        kitty
        mpv
        zathura
	waybar
	yambar
        river
	dwl
        jetbrains.idea-ultimate
	jetbrains.rider
        docker-compose
        obs-studio
        grim
        slurp
        mako
	dunst
	bc
        swaylock
        swaybg
	bemenu
	wl-clipboard
	pamixer
	pavucontrol
	playerctl
	brightnessctl
	networkmanagerapplet
	lxappearance
	papirus-icon-theme
	numix-cursor-theme
	rustup
	rust-analyzer
	adoptopenjdk-icedtea-web
	jdk11
	dotnet-sdk
    ];

    # Enable OpenGL
    hardware.opengl.enable = true;

    # Enable networkmanager
    networking.networkmanager.enable = true;

    # Use Chrony to sync time
    services.chrony.enable = true;

    # Enable cpufreq (laptop only)
    services.auto-cpufreq.enable = true;

    # Enable zsh system-wide
    programs.zsh.enable = true;

    # Enable Docker
    virtualisation.docker.enable = true;

    # Replace sudo with doas
    security.doas = {
	enable = true;
	extraRules = [
		{ groups = [ "wheel" ]; persist = true; }
	];
    };

    # Variables and tings 
    environment.variables = {
    	# Set default apps
    	EDITOR = "nvim";
	TERMINAL = "kitty";
	BROWSER = "firefox";
	READER = "zathura";
	
	# Clean up home dir
	XDG_CONFIG_HOME = "\${HOME}/.config";
	XDG_DATA_HOME = "\${HOME}/.local/share";
	XDG_CACHE_HOME = "\${HOME}/.cache";
	XINITRC = "\${XDG_CONFIG_HOME}/x11/xinitrc";
	CARGO_HOME = "\${XDG_DATA_HOME}/cargo";
	RUSTUP_HOME = "\${XDG_DATA_HOME}/rustup";
	HISTFILE = "\${XDG_DATA_HOME}/history";
	GTK2_RC_FILES = "\${XDG_CONFIG_HOME}/gtk-2.0/gtkrc-2.0";
	GOPATH = "\${XDG_DATA_HOME}/go";

	# Program settings
	FZF_DEFAULT_OPTS = "--layout=reverse --height 40%";
	JDTLS_HOME = "/usr/share/java/jdtls";
	
	# Smooth scrolling in Firefox
	MOZ_USE_XINPUT2 = "1";
	MOZ_ENABLE_WAYLAND = "1";
	
	# Fix Java applications in WMs
	_JAVA_AWT_WM_NONREPARENTING = "1";
	_JAVA_OPTIONS = "-Dawt.useSystemAAFontSettings=lcd";
	
	# Hidpi
	GDK_SCALE = "1";
	QT_SCALE_FACTOR = "1";
    };
}
