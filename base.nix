{ config, pkgs, ... }:

{
    # Install latest kernel available
    boot.kernelPackages = pkgs.linuxPackages_latest;

    # Basic
    networking.hostName = "nsa-mainframe";
    time.timeZone = "Europe/Copenhagen";

    # Locale and set tty lang
    i18n.defaultLocale = "en_DK.UTF-8";
    console.keyMap = "dk";

    # Enable printing
    services.printing.enable = true;

    # Sound (Pipewire)
    hardware.pulseaudio.enable = false;
    security.rtkit.enable = true;
    services.pipewire = {
        enable = true;
        alsa.enable = true;
        alsa.support32Bit = true;
        pulse.enable = true;
        jack.enable = true;

	# replace media-session with wireplumber
	media-session.enable = false;
	wireplumber.enable = true;
    };

    # Enable Nix command
    nix = {
	extraOptions = ''
      		experimental-features = nix-command flakes
    	'';
    };
}
