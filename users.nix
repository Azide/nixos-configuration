{ config, pkgs, ... }:

{
    users.users.marcus = {
        isNormalUser = true;
        extraGroups = [ "wheel" "video" "audio" "docker" "networkmanager" ];
        shell = pkgs.zsh;
    };
}
